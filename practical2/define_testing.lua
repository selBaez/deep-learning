require 'torch'   -- torch
require 'xlua'    -- xlua provides useful tools, like progress bars
require 'optim'   -- an optimization package, for online and batch methods

----------------------------------------------------------------------
--print '==> Printing on the test set.'

-- test function
function test()
   -- local vars
   local time = sys.clock()
   local lossTotal = 0 -- the accumulated loss for all batches in the epoch

   -- averaged param use?
   if average then
      cachedparams = parameters:clone()
      parameters:copy(average)
   end

   -- set model to evaluate mode (for modules that differ in training and testing, like Dropout)
   model:evaluate()

   -- test over test data
   for t = 1,testData:size() do
      -- disp progress
      --xlua.progress(t, testData:size())

      -- get new sample
      local input = testData.data[t]
      if opt.type == 'double' then input = input:double()
      elseif opt.type == 'cuda' then input = input:cuda() end
      local target = testData.labels[t]

      -- test sample
      local pred = model:forward(input)
      pred_1 = torch.reshape(pred, 10)
      confusion:add(pred_1, target)
        
      local err = criterion:forward(pred, target)
      lossTotal = lossTotal + err
   end

   -- timing
   time = sys.clock() - time
   time = time / testData:size()
   print("\n==> time to test 1 sample = " .. (time*1000) .. 'ms')

   -- print confusion matrix
   --if (epoch == (max_epoch + 1)) then
   print(confusion)
   --end

   -- update log/plot
   testLogger:add{['% mean class accuracy (test set)'] = confusion.totalValid * 100}
   testErrorLogger:add{['% mean error (test set)'] = lossTotal/testData:size()}
   if opt.plot then
      testLogger:style{['% mean class accuracy (test set)'] = '-'}
      testLogger:plot()
   end

   -- averaged param use?
   if average then
      -- restore parameters
      parameters:copy(cachedparams)
   end
   
   -- next iteration:
   confusion:zero()
end
