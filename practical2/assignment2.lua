----------------------------------------------------------------------
-- Section 2.1
----------------------------------------------------------------------
require 'torch'
require 'image'
require 'nn'
require 'optim'
Plot = require 'itorch.Plot'

------------------------------------------------------
print '=== I. ENVIRONMENT PARAMETERS ==='
torch.setdefaulttensortype('torch.FloatTensor')
torch.setnumthreads(1)
torch.manualSeed(1)

------------------------------------------------------
print '=== II. EXPERIMENT OPTIONS ==='
opt                       = {}
opt['loss']               = nn.MultiMarginCriterion() -- TODO
-- criterion = nn.MultiMarginCriterion()
-- criterion = nn.ClassNLLCriterion()
-- Other loss choices:
-- For classification: ClassNLLCriterion
-- For regression: MSECriterion
-- For more options: https://github.com/torch/nn/blob/master/doc/criterion.md
opt['id']                 = 'MultiMarginCriterion_sgd_deep_wide_relu_wd001'
opt['save']               = paths.concat('experiments/', (opt.id .. '/'))
opt['optimization']       = optim.sgd -- TODO
-- Other optimization methods:
-- ASGD (Good for very large datasets and training over multiple machines)
-- CG (Compute full gradients, no mini-batch updates)
-- LBFGS (Compute full gradients, no mini-batch updates)
-- For more options: http://optim.readthedocs.org/en/latest/
opt['small']              = {} -- {10000, 5000}
opt['train_log']          = 'train.log'
opt['test_log']           = 'test.log'
opt['train_error_log']    = 'trainError.log'
opt['test_error_log']     = 'testError.log'

------------------------------------------------------
print '=== III. MODEL OPTIONS ==='
model_opt                 = {}
model_opt['nhidden']      = 200 -- TODO
model_opt['batchSize']    = 1 -- TODO
model_opt['learningRate'] = 0.001 -- TODO
model_opt['weightDecay']  = 0.001 -- TODO
model_opt['momentum']     = 0.0 -- TODO]]

----------------------------------------------------------------------
-- Section 2.1
----------------------------------------------------------------------
------------------------------------------------------
print '=== A. LOAD DATA ==='
dofile 'load_data_mnist.lua'

------------------------------------------------------
print '=== B. PREPROCESS DATA ==='
dofile 'preprocess_data_mnist.lua'

------------------------------------------------------
print '=== C. DEFINE MODEL ==='
dofile 'define_model.lua'

------------------------------------------------------
print '=== D. DEFINE TRAINING ==='
dofile 'define_training.lua'

------------------------------------------------------
print '=== E. DEFINE TESTING ==='
dofile 'define_testing.lua'

----------------------------------------------------------------------
print '=== RUN THE EXPERIMENT ==='

-- TODO If you want add a maximum number of epochs that you allow for the training.
epoch = 1
max_epoch = 50

for epochs = 1, max_epoch do
    train()
    test()
end

-----------------------------------------------------------------
print '=== PLOT RESULTS ==='
dofile 'plotting.lua'

plotResults((opt.id .. ' accuracy'), paths.concat(opt.save, opt.train_log), paths.concat(opt.save, opt.test_log), max_epoch)
plotResults((opt.id .. ' loss'), paths.concat(opt.save, opt.train_error_log), paths.concat(opt.save, opt.test_error_log), max_epoch)

