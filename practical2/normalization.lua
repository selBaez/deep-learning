require 'torch'
Plot = require 'itorch.Plot'

-- Load the points
local n = 100
local data = torch.Tensor(n, 2)
local n_counter = 1 

local file = io.open('points.txt')
if file then
    for line in file:lines() do
        data[n_counter][1], data[n_counter][2] = unpack(line:split(" "))
        n_counter = n_counter+1
    end
else
end

-- Get mean and standard deviation per dimension
local mean = torch.mean(data, 1)
print ('Mean on X-axis ' .. mean[1][1] .. ' and Y-axis' .. mean [1][2])
local std = torch.std(data, 1)
print('Standard deviation on X-axis ' .. std[1][1] .. ' and Y-axis' .. std [1][2])

-- Gaussian Normalization
local normalized_x = torch.div((data[{{},1}] - mean[1][1]), std[1][1])
local normalized_y = torch.div((data[{{},2}] - mean[1][2]), std[1][2])
--print(torch.cat(torch.cat(data, normalized_x, 2), normalized_y, 2))

-- Get mean and standard deviation per dimension after normalization 
local mean = torch.mean(torch.cat(normalized_x, normalized_y, 2), 1)
print ('Mean after normalization on X-axis ' .. mean[1][1] .. ' and Y-axis ' .. mean [1][2])
local std = torch.std(torch.cat(normalized_x, normalized_y, 2), 1)
print('Standard deviation after normalization on X-axis ' .. std[1][1] .. ' and Y-axis ' .. std [1][2])

-- Plotting
Plot():circle(data[{{},1}], data[{{},2}],'green'):circle(normalized_x, normalized_y, 'blue'):title('Data points'):draw()

    
        