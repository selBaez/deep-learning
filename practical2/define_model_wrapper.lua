require 'nn'
--require 'MyModules/MyRequ'
--require 'MyModules/MySin'

function define_model_wrapper(D, nhidden, C)
  ------------------------------------------------------------------------------
  -- MODEL
  ------------------------------------------------------------------------------
  
  -- OUR MODEL:
  --     linear -> sigmoid/requ -> linear -> softmax
    
    print '==> Define the model'

    local model = nn.Sequential()
    model:add(nn.Reshape(D))
    model:add(nn.Linear(D, nhidden))
    -- model:add(nn.MyRequ())
    model:add(nn.Sigmoid())
    model:add(nn.Linear(nhidden, C))
    model:add(nn.LogSoftMax())

  ------------------------------------------------------------------------------
  -- LOSS FUNCTION
  ------------------------------------------------------------------------------

  -- Practically copy paste your code from the script file
    
    print '==> Define loss'
    
    criterion = nn.MultiMarginCriterion()

  return model, criterion
end

return define_model_wrapper

