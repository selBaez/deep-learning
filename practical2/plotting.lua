require 'torch'
Plot = require 'itorch.Plot'

    
function plotResults(title, train_log, test_log, epochs)
    
    -- Load the points
    local data = torch.Tensor(epochs, 2)
    local n_counter = 0 

    local train_file = io.open(train_log)
    local test_file = io.open(test_log)

    if train_file then  
        for line in train_file:lines() do
            if n_counter ~= 0 then
                data[n_counter][1] = unpack(line:split(" "))
            else
                disregard = line
            end
            n_counter = n_counter+1
        end
    else
    end

    n_counter = 0

    if test_file then
        for line in test_file:lines() do
            if n_counter ~= 0 then
                data[n_counter][2] = unpack(line:split(" "))
            else
                disregard = line
            end
            n_counter = n_counter+1
        end
    else
    end

    xrange = torch.linspace(1, epochs, epochs)

    -- Plotting and saving
    local graph = Plot():circle(xrange, data[{{},1}],'green', 'train'):circle(xrange, data[{{},2}],'blue', 'test'):legend(true):xaxis('Epochs'):title(title):draw() 
    graph:save(paths.concat(opt.save, title))
    
end